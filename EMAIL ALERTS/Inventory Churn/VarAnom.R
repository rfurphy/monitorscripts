

library(dplyr)
library(RODBC)

## get raw model data 
db.PM <- odbcDriverConnect('driver={SQL Server};server=SP-ATT-PMAPP-02\\PMPROD,1435;database=PredictiveModeling;trusted_connection=true')
d.churn <- sqlQuery(db.PM, 
                    'SELECT 
                    *
                    FROM PredictiveModeling.dbo.ModelScoreCompletedRecord_30
                    WHERE ScoreCalculatedDateTime >= DATEADD(Day, -46, getdate()) ')



## add in interaction terms 
d.churn <- d.churn %>% 
  mutate(`NDaysCoreInventory_7Days*SqrtRelVDP_Rate_7Days` = NDaysCoreInventory_7Days*SqrtRelVDP_Rate_7Days, 
         `AvgLock_Rate_7Days*DaysToFirstSale` = AvgLock_Rate_7Days*DaysToFirstSale, 
         `NDaysCoreInventory_7Days*RelVDP_Rate_7Days` = NDaysCoreInventory_7Days*RelVDP_Rate_7Days,
         `NDaysCoreUnlockedInventory_7Days*NDaysCoreInventory_7Days` = NDaysCoreUnlockedInventory_7Days*NDaysCoreInventory_7Days,
         `IsOnDemand_FVSH*WebsiteInventory_MMPriceGrp` = IsOnDemand_FVSH*WebsiteInventory_MMPriceGrp) %>% 
  mutate(ScoreCalculatedDateTime = as.Date(ScoreCalculatedDateTime))

## separate data 
data.today <- d.churn %>% filter(ScoreCalculatedDateTime == Sys.Date())
data.past <- d.churn %>% filter(ScoreCalculatedDateTime != Sys.Date())


## get 45 days of means 
means.past <- d.churn %>% 
  group_by(ScoreCalculatedDateTime) %>% 
  summarise_all(funs(mean))


## only feed in numeric 
data.today <- data.today %>% 
  select_if(is.numeric) %>% 
  select(-ModelScoreKey,
         -ModelVersionID,
         -StockNumber,
         -ScoreProcedureID,
         -Realization,
         -Score)
data.past <- data.past %>% 
  select_if(is.numeric) %>% 
  select(-ModelScoreKey,
         -ModelVersionID,
         -StockNumber,
         -ScoreProcedureID,
         -Realization,
         -Score)


## identify problems 
problems <- function(data.today, data.past){
  mean.today <- data.today %>% 
    summarise_all(funs(mean))
  mean.past <- data.past %>% 
    summarise_all(funs(mean))
  sd.past <- data.past %>% 
    summarise_all(funs(sd))
  
  p <- c()
  for(name in names(mean.today)){
    if(mean.today[[name]][1] > mean.past[[name]][1] + 2* sd.past[[name]][1] | mean.today[[name]][1] < mean.past[[name]][1] - 2* sd.past[[name]][1])
      p <- c(p, name)
  }
  
  return(p)
}
p <- problems(data.today, data.past)


if(!is.null(p)){
  ## create dummy data for efficient storage 
  p_data.temp <- data.past[, p]
  out_data <- list()
  for(i in 1:length(p)){
    # edges <- quantile(p_data.temp[,i], probs = c(0.075, 0.25, 0.5, 0.75, 0.925))
    # temp_data <- list()
    # for(j in 2:length(edges)){
    #   vals <- runif(100, min=edges[j-1], max=edges[j])
    #   temp_data[[j]] <- data.frame(VariableName = rep(p[i], times = 101),
    #                                VariableValue = c(edges[j-1], vals),
    #                                VariableType = c(names(edges)[j-1], rep("dummy", times = 100)))
    # }
    # out_data[[i]] <- do.call(rbind, temp_data)
    # out_data[[i]]$VariableType <- as.character(out_data[[i]]$VariableType)
    # out_data[[i]] <- rbind(out_data[[i]], c(p[i], edges[length(edges)], names(edges)[length(edges)]))
    # out_data[[i]]$DateClassification = "past"
    # out_data[[i]] <- rbind(out_data[[i]], c(p[i], mean.today[[p[i]]][1], "outlier", "today"))
    out_data[[i]] <- data.frame(VariableName = rep(p[i], times = 46),
                            VariableValue = c(means.past[[p[i]]]),
                            VariableType = seq(from = 45, to = 0, by = -1),
                            DateClassification = c(rep("past", times = 45), "today"))
    
  }
  out_data <- do.call(rbind, out_data)
  
  
  ## prep for SQL storage
  out_data$VariableValue <- as.numeric(out_data$VariableValue)
  out_data$DateGenerated <- as.character(Sys.Date())
  out_data$VariableName <- as.character(out_data$VariableName)
  
  out_data[is.na(out_data)] <- "NULL"
  getVAID <- sqlQuery(db.PM, "select max(VAID) as vaidmax from [dbo].[ModelScoreFeatureAnomalies_30]")
  if(is.na(getVAID[1,1])){
    getVAID[1,1] <- 0
  }
  startID <- getVAID[1,1]
  out_data <- out_data %>%
    mutate(VAID = row_number() + startID,
           VariableName = ifelse(grepl("\\*", VariableName), gsub("\\*", "_x_", VariableName), VariableName),
           VariableName = paste("\'", VariableName, "\'", sep = ""),
           DateGenerated = paste("\'", DateGenerated, "\'", sep = ""),
           VariableType = paste("\'", VariableType, "\'", sep = ""),
           DateClassification = paste("\'", DateClassification, "\'", sep = ""))
  out_data <- out_data[,c(6,1,2,3,4,5)]
  
  ## insert into SQL table
  insertstatement <- sprintf(
    'SET NOCOUNT ON; SET IDENTITY_INSERT [dbo].[ModelScoreFeatureAnomalies_30] ON; INSERT INTO [dbo].[ModelScoreFeatureAnomalies_30] (VAID, VariableName, VariableValue, VariableType, DateClassification, DateGenerated) VALUES (%s)',
    apply(out_data, 1, function(i) paste(i, collapse=","))
  )
  
  lapply(insertstatement, function(s) sqlQuery(db.PM, s))
}


