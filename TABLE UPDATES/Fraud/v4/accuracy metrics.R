

library(RODBC)
library(dplyr)
library(DBI) 
library(pROC)
library(lubridate)

db.PM <- odbcDriverConnect('driver={SQL Server};server=PROD-PREMOD\\PMPROD,1435;database=PredictiveModeling;trusted_connection=true')
db.DWHS <- odbcDriverConnect('driver={SQL Server};server=DwhCar-RO\\DataWarehouseCar;database=CarvanaDWHS;trusted_connection=true')
db.EDW <- odbcDriverConnect('driver={SQL Server};server=SL-ATT-ENG-P-01, 1439;database=EDW;trusted_connection=true')

d.FSv4 <- sqlQuery(db.PM, 
                   "SELECT s.UserID, s.Score, f.Flag
FROM(
                   SELECT DISTINCT b.UserID
                   , MAX(b.Score) AS Score
                   FROM dbo.ModelScoreCompletedRecord_49 b 
                   WHERE b.DependentVariableName = 'Score'
                   GROUP BY b.UserID
) s 
                   JOIN (
                   SELECT DISTINCT b.UserID
                   , MAX(b.Score) AS Flag
                   FROM dbo.ModelScoreCompletedRecord_49 b 
                   WHERE b.DependentVariableName = 'Flag'
                   GROUP BY b.UserID
                   ) f
                   ON f.UserID = s.UserID")

d.SFFraud <- sqlQuery(db.DWHS, 
                      "SELECT DISTINCT a.UserID, c.Status
                        FROM CarvanaDWHS.sfdc.tblCase AS c WITH (NOLOCK)
                        LEFT JOIN CarvanaDWHS.sfdc.Account AS a WITH (NOLOCK)
                            ON a.Id = c.AccountId
                        WHERE c.RecordTypeName = 'Potential Fraud' AND c.Status = 'Closed - Is Fraud'")

d.Bad <- sqlQuery(db.DWHS, 
                  "SELECT u.UserID
                    	, MAX(CASE WHEN (d.Ever30 = 1 AND d.Ever30Days < 64) OR (d.EverCO = 1 AND d.DaystoChargeOff < 154) -- FPD or CO 
                    		  THEN 1 ELSE 0 END) AS Bad
                    FROM FinanceGroup.dbo.tblLoanTape AS l WITH (NOLOCK)
                    LEFT JOIN FinanceGroup.dbo.tblEverDQ AS d WITH (NOLOCK)
                    	ON d.AccountNumber = l.AccountNumber
                    LEFT JOIN CarvanaDWHS.finance.FactLoanBorrower f WITH (NOLOCK)
                    	ON f.AccountNumber = l.AccountNumber
                    LEFT JOIN CarvanaDWHS.dw.FactSale s WITH (NOLOCK)
                    	ON s.SaleSK = f.SaleSK
                    LEFT JOIN CarvanaDWHS.dw.DimUser u WITH (NOLOCK)
                    	ON u.UserSK = s.UserSK
                    LEFT JOIN CarvanaDWHS.dw.DimPurchase p WITH (NOLOCK)
                    	ON s.PurchaseSK = p.PurchaseSK
                    GROUP BY u.UserID")

d.OP <- sqlQuery(db.EDW, 
                 "SELECT UserID,
                  PurchaseID,
                  OccurredOnUTC as FirstOrderPlacedDateTimeUTC,
                  PaymentType, 
SoftPullFICO
                   FROM
                     (
                     SELECT EventOrdinal,
                       PurchaseEventName,
                       OccurredOnUTC,
                       CustomerID AS UserID,
                       PurchaseID,
                       PaymentType,
                       SoftPullFICO,
                       ROW_NUMBER() OVER (PARTITION BY CustomerID ORDER BY OccurredOnUTC) rw
                     FROM EDW.dbo.Purchase_v AS p WITH (NOLOCK)
                     WHERE PurchaseEventName = 'OrderPlaced' 
                   ) AS p2
                   WHERE rw = 1 AND OccurredOnUTC >= '2019-07-01'")

d.Comparison <- d.FSv4 %>% 
  left_join(d.SFFraud, by = "UserID") %>% 
  mutate(RF = ifelse(is.na(Status), 0, 1)) %>% 
  select(UserID, Score, Flag, RF)
d.Comparison2 <- left_join(d.OP %>% select(UserID, FirstOrderPlacedDateTimeUTC, SoftPullFICO), d.Comparison, 
                           by = "UserID") %>% 
  mutate(FirstOrderPlacedDateTimeUTC = as.POSIXct(FirstOrderPlacedDateTimeUTC, tz = "UTC", origin = "1970-01-01"), 
         week = floor_date(FirstOrderPlacedDateTimeUTC, unit = "week"), 
         month = floor_date(FirstOrderPlacedDateTimeUTC, unit = "month"),
         TP_RF = ifelse(Flag == 1 & RF == 1, 1, 0),
         TN_RF = ifelse(Flag == 0 & RF == 0, 1, 0), 
         FP_RF = ifelse(Flag == 1 & RF == 0, 1, 0), 
         FN_RF = ifelse(Flag == 0 & RF == 1, 1, 0), 
         Prime = ifelse(SoftPullFICO >= 600 & SoftPullFICO < 9000, "Prime", "SubPrime"))
d.Comparison3 <- left_join(d.Comparison2, d.Bad, 
                           by = "UserID") %>% 
  mutate(Bad = ifelse(is.na(Bad), 0, Bad), 
         TP_Bad = ifelse(Flag == 1 & Bad == 1, 1, 0),
         TN_Bad = ifelse(Flag == 0 & Bad == 0, 1, 0), 
         FP_Bad = ifelse(Flag == 1 & Bad == 0, 1, 0), 
         FN_Bad = ifelse(Flag == 0 & Bad == 1, 1, 0)) %>% 
  mutate(TP_All = ifelse(Flag == 1 & (Bad == 1 | RF == 1), 1, 0),
         TN_All = ifelse(Flag == 0 & (Bad == 0 | RF == 1), 1, 0), 
         FP_All = ifelse(Flag == 1 & (Bad == 0 | RF == 1), 1, 0), 
         FN_All = ifelse(Flag == 0 & (Bad == 1 | RF == 1), 1, 0), 
         All = ifelse(Bad == 1 | RF == 1, 1, 0)) %>% 
  filter(!is.na(Score) & !is.na(Prime))

d.Summary <- d.Comparison3 %>% 
  mutate(RF = as.factor(RF)) %>% 
  filter(Prime == "Prime") %>% 
  group_by(month, Prime) %>% 
  summarise(#auc_RF = auc(RF, Score), 
            precision_RF = sum(TP_RF, na.rm=T)/(sum(TP_RF, na.rm=T) + sum(FP_RF, na.rm=T)),  
            recall_RF = sum(TP_RF, na.rm=T)/(sum(TP_RF, na.rm=T) + sum(FN_RF, na.rm=T)), 
            #auc_Bad = auc(Bad, Score), 
            precision_Bad = sum(TP_Bad, na.rm=T)/(sum(TP_Bad, na.rm=T) + sum(FP_Bad, na.rm=T)),  
            recall_Bad = sum(TP_Bad, na.rm=T)/(sum(TP_Bad, na.rm=T) + sum(FN_Bad, na.rm=T)), 
            #auc_All = auc(All, Score), 
            precision_All = sum(TP_All, na.rm=T)/(sum(TP_All, na.rm=T) + sum(FP_All, na.rm=T)),  
            recall_All = sum(TP_All, na.rm=T)/(sum(TP_All, na.rm=T) + sum(FN_All, na.rm=T))) 



dbi.PM <- dbConnect(odbc::odbc(),
                    Driver = "SQL Server",
                    Server = "PROD-PREMOD\\PMPROD,1435",
                    Database = "PredictiveModeling",
                    Trusted_Connection = "True")
tbl <- Id(schema = "adhoc", table = "Furphy_FSv4Accuracy")
dbWriteTable(dbi.PM,
             tbl,
             d.Summary,
             overwrite = TRUE)
